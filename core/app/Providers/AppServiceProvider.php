<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $type = 'blade';
        foreach (['js', 'css'] as $t) {
            View::addExtension("$type.$t", $type);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(){}
}
