<!DOCTYPE html>
<html>
  <head>
    @include('index.head')
  </head>
  <body>
    @include('index.notification')
    @include('index.header')
    @include('index.highlights')
    @include('index.footer')
    @include('index.newsletter')

    <script src="{{ asset(
      '/tp/jquery-3.3.1.min.js'
    ) }}"></script>
    <script>
      @include('script')
    </script>
  </body>
</html>
