<div id="elHeader">
  <img class="logo" src="{{ asset(
    '/img/y-logo-white.png'
  ) }}" >
  <div class="headline">
    <h1>Hello! I'm Dwi Prabowo</h1>
    <h2>Consult, Design, and Develop Websites</h2>
    <p>
      Have something great in mind? Feel free to contact me.
      I'll help you to make it happen.
    </p>
    <button>LET'S MAKE CONTACT</button>
  </div>
</div>
