@php
  $highlights = [
    [
      'title' => 'Consult',
      'icon' => 'far fa-comments',
      'content' => "Co-create, design thinking; strengthen infrastructure resist granular. Revolution circular, movements or framework social impact low-hanging fruit. Save the world compelling revolutionary progress.",
    ],
    [
      'title' => 'Design',
      'icon'  => 'fas fa-paint-brush',
      'content' => 'Policymaker collaborates collective impact humanitarian shared value vocabulary inspire issue outcomes agile. Overcome injustice deep dive agile issue outcomes vibrant boots on the ground sustainable.',
    ],
    [
      'title' => 'Develop',
      'icon'  => 'fas fa-cubes',
      'content' => 'Revolutionary circular, movements a or impact framework social impact low- hanging. Save the compelling revolutionary inspire progress. Collective impacts and challenges for opportunities of thought provoking.',
    ],
    [
      'title' => 'Marketing',
      'icon'  => 'fas fa-bullhorn',
      'content' => 'Peaceful; vibrant paradigm, collaborative cities. Shared vocabulary agile, replicable, effective altruism youth. Mobilize commitment to overcome injustice resilient, uplift social transparent effective.',
    ],
    [
      'title' => 'Manage',
      'icon'  => 'fas fa-tasks',
      'content' => 'Change-makers innovation or shared unit of analysis. Overcome injustice outcomes strategize vibrant boots on the ground sustainable. Optimism, effective altruism invest optimism corporate social.',
    ],
    [
      'title' => 'Evolve',
      'icon'  => 'fas fa-chart-line',
      'content' => 'Activate catalyze and impact contextualize humanitarian. Unit of analysis overcome injustice storytelling altruism. Thought leadership mass incarceration. Outcomes big data, fairness, social game-changer.',
    ],
  ];
@endphp
<div class="highlights">
  <div class="header">
    <h2>How Can I Help You?</h2>
    <p>
      Our work then targeted, best practices outcomes social innovation synergy.
      <br/>
      Venture philanthropy, revolutionary inclusive policymaker relief. User-centered program areas scale.
    </p>
  </div>
  <div class="items">
  @foreach ($highlights as $item)
    <div class="item">
      <div class="inner">
        <h3>{{ $item['title'] }}</h3>
        <div class="icon">
          <i class="{{ $item['icon'] }}"></i>
        </div>
        <p>{{ $item['content'] }}</p>
      </div>
    </div>
  @endforeach
  </div>
</div>
