<form>
  <h2>Get latest updates in web technologies</h2>
  <p>
     I write articles related to web technologies, such as design trends, development tools, UI/UX case studies and reviews, and more. Sign up to my newsletter to get them all.
  </p>
  <div class="wrapper">
    <div class="row">
      <div class="cell">
        <input type="email" placeHolder="Email address" />
      </div>
      <div class="cell button">
        <button>Count me in!</button>
      </div>
    </div>
  </div>
  <div id="closeForm">
    <i class="fas fa-times"></i>
  </div>
</form>
