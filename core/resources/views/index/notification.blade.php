<div class="elNotification real">
  <div class="content">
    By accessing and using this website, you acknowledge that you have read and
    understand our <a href="#">Cookie Policy</a>, <a href="#">Privacy Policy</a>, and our <a href="#">Terms of Service</a>.
    <button>Got it</button>
  </div>
</div>
<div class="elNotification fake">
  <div class="content">
    By accessing and using this website, you acknowledge that you have read and
    understand our <a href="#">Cookie Policy</a>, <a href="#">Privacy Policy</a>, and our <a href="#">Terms of Service</a>.
    <button>Got it</button>
  </div>
</div>
