$(function() {

  @include('index.script.cookie');

  $(".elNotification .content button").click(function() {
    $(".elNotification").slideUp();
    $('html,body').animate({ scrollTop: 0 });
  });

  var formHeight = $('form').outerHeight();
  $(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    var height = $('html').height();
    if (height/3 < scroll && !getCookie('closeForm')) {
      $('form').animate({top: $(window).height() - formHeight});
      $('form input').focus();
    }
  });

  $("#closeForm").click(function() {
    $("form")
      .stop(true,false).animate({top: $(window).height()});
    setCookie('closeForm', true, 10);
  });
});
