Dear Dwi,

Thank you for applying to Cermati as Frontend Developer. We appreciate your time. FYI, we have a written assignment for you as part of our recruitment process. We hope you don't mind to finish the test before we can proceed with the interviews. Please let us know if you have trouble finishing the assignment because of time constraints or if you're sick or if there are any circumstances that causes you to not be able to finish the assignment. We might be able to accommodate some changes. 

For now here's the written test (it's attached in this email)

Please complete this test and submit your answers by reply this email within 5 (five) days starting from the date this email is sent. If you have any questions, please contact us through email or call us at (021)22561844.

Good luck and have fun! 

Sincerely,

Cermati Recruitment Team
