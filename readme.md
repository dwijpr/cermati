
Read me
------------------------------------------------------------------------------

text based documents:
1. README.txt - applied here see the [Cermati.com Front-end Developer Test] section
2. Front-end Developer Excercise.pdf

## ToDo
Compare the documents
Front-end Developer Excercise.pdf is the main document

## Stories

Your main task is to create a web page based on the following mockup (Figure 1) using
your own code (​ please don’t use any template or theme​ ). You have to make it as similar as
possible to the mockup. The full-size mockup, related assets, and content text
(copywriting) are provided for your reference at the end section of this document.

--------------------------------

As shown in Figure 1, the web page consists of the following elements:

1. Notification Panel

  The notification panel (see Figure 2 and Figure 3) consists of paragraph and a
  button; the content (the paragraph including the button) wide should not be more
  than 720px. This panel should ​ stay on top of the page​ as the page is scrolled
  down/up. If the button is clicked, the panel will be ​ animated sliding up​ - not just
  suddenly disappears - and also the page content follows sliding up to the top of the
  page.

2. Page Header and Hero-shot

  The page header only consists of a logo, positioned at top-left corner. The
  hero-shot contains headline, paragraph, and a call-to-action button.

  Please write your name in the headline with the following format: “​ Hello! I’m
  {your-name}​ ”, e.g. “​ Hello! I’m Yuan Monos​ ”. Then, for the call-to-action button, consider the changes on hover state as the following details (Figure 5):

3. Highlights Panel

  The highlights panel consists of section header, leading paragraph, and 6 (six) tiles
  of content. In each tile contains icon, title, and a paragraph/description. The icons
  set can use those provided from FontAwesome [ https://fontawesome.com/ ].

  The tiles layout should be responsive, scheme could be seen in Figure 6 below,
  arranged with the following orders:

    1. Layout of 2 row x 3 column tiles for the screen size wider than 960px
    2. Layout of 3 row x 2 column tiles for the screen size between 480px and 960px
    3. Stack of tiles for the screen size narrower than 480px

4. Page Footer

  The page footer (Figure 7) only consist of copyright info with the following format:
  copyright icon, year, your name, then the “​ All rights reserved.​ ” text, e.g. “​ © 2018
  Yuan Monos. All rights reserved.​ ”

5. Newsletter Panel

  The newsletter panel (see Figure 8) contains newsletter subscription form but ​ the
  form doesn’t have to work properly (you are not required to handle POST/GET
  request)​ . Initially, the panel is hidden then it should show up by sliding from bottom
  to up properly. Here are the specifications for the newsletter panel:

  a. This panel consists of headline/title, paragraph, form with email input field
  and submit button, and also close button. The panel size should not be more
  than 640px wide.
  
  b. This panel should be shown only after scrolling down to about ​ one-third​ of
  the page and must be ​ animated sliding up​ - not just suddenly appears. After
  being shown, this panel keeps on showing/visible even when scrolling up
  back to the top of the page.

  c. This panel should be placed at the ​ bottom-left corner o
  f the screen ​ and stay
  there​ as the page is scrolled down/up.

  d. This panel could be hidden/removed by clicking the close button on top-right
  of the panel. When it’s being closed it should be ​ animated sliding down​ and
  also it should ​ remember to keep being hidden​ for 10 minutes after. So after
  the panel is closed, it will not showing up again whether the page is scrolled
  up/down or when the page is reloaded.

**Notes**

Some extra notes to be considered:

1. Write your name in the web page title using the following format:

  {your-name} | Cermati.com Front-end Developer Entry Test
  example:
  Yuan Monos | Cermati.com Front-end Developer Entry Test

2. For the color scheme, you may use the following colors as the basic color palette.

  ○ Blue: #
  007bc1
  ○ Dark blue: ​ #004a75
  ○ Orange: ​ #ff8000
  ○ Dark orange: #
  cc6600
  ○ Smoke Grey: #
  e5e5e5

3. The web page could be shown/accessed using big screen, such as monitor with
resolution of 1920 x 1080 px, you have to maintain and/or optimize the web page
content width not more than 1366px with center alignment of the screen.

4. The web page should be compatible​ for cross-browser testing​ . As for this exercise,
please make sure (at least) it works on Google Chrome, Mozilla Firefox, and
Android browsers.

5. The deliverable could be sent back using the following methods:

  ○ Email attachment using compressed file (ZIP format)
  ○ Link to your files/folder in Google Drive ~ ​ preferable
  ○ Link to your online repository, such as GitHub, etc.

-----------

It’s allowed for you to use other online resources as references, such as Google, stack
overflow, etc. to help you in doing this exercise. You may also check www.cermati.com
and/or engineering.cermati.com as reference or inspiration to do this exercise.
Please read, understand, and follow the specifications carefully​ . Every detail does matter,
so do your best and may the force be with you. Good luck!
NOTE:​ All the required assets and references are available as separate attachment.

---------

files:
1. The full-size mockup 	---> cermati-test~full-mockup~desktop-w1366.jpg, cermati-test~full-mockup~mobile-w360.jpg
2. related assets		---> work-desk~~dustin-lee.jpg, y-logo-white.png
3. content text (copywriting)	---> README.txt

------------------------------------------------------------------------------

Cermati.com Front-end Developer Test
-------------------------------------------------

First of all, thank you for applying to Cermati.com for the front-end developer
position. Here are some files provided for the entry test:
0. `README.txt` - general information and webpage content text reference
1. `cermati-test_full-mockup_desktop-w1366.jpg` - webpage mockup (desktop version)
2. `cermati-test_full-mockup_mobile-w360.jpg` - webpage mockup (mobile version)
3. `work-desk__dustin-lee.jpg` - background image for the hero-shot section;
     original image by Dustin Lee (@dustinlee) provided in Unsplash.com
4. `y-logo-white.png` - logo icon



CONTENT TEXT
=====
The following is used for the content text (copywriting) of the webpage.

[ ~~~ Notification Panel ~~~ ]
By accessing and using this website, you acknowledge that you have read and
understand our Cookie Policy, Privacy Policy, and our Terms of Service.


[ ~~~ Hero-shot Section ~~~ ]
Hello! I'm {your-name}
Consult, Design, and Develop Websites
Have something great in mind? Feel free to contact me.
I'll help you to make it happen.
Let's Make Contact


[ ~~~ Highlights Panel ~~~ ]
How Can I Help You?
Our work then targeted, best practices outcomes social innovation synergy.
Venture philanthropy, revolutionary inclusive policymaker relief. User-centered
program areas scale.

  Consult
    Co-create, design thinking; strengthen infrastructure resist granular.
    Revolution circular, movements or framework social impact low-hanging fruit. 
    Save the world compelling revolutionary progress.

  Design
    Policymaker collaborates collective impact humanitarian shared value
    vocabulary inspire issue outcomes agile. Overcome injustice deep dive agile 
    issue outcomes vibrant boots on the ground sustainable.

  Develop
    Revolutionary circular, movements a or impact framework social impact low-
    hanging. Save the compelling revolutionary inspire progress. Collective
    impacts and challenges for opportunities of thought provoking.

  Marketing
    Peaceful; vibrant paradigm, collaborative cities. Shared vocabulary agile,
    replicable, effective altruism youth. Mobilize commitment to overcome
    injustice resilient, uplift social transparent effective.

  Manage
    Change-makers innovation or shared unit of analysis. Overcome injustice
    outcomes strategize vibrant boots on the ground sustainable. Optimism,
    effective altruism invest optimism corporate social.

  Evolve
    Activate catalyze and impact contextualize humanitarian. Unit of analysis
    overcome injustice storytelling altruism. Thought leadership mass 
    incarceration. Outcomes big data, fairness, social game-changer.


[ ~~~ Sliding Panel ~~~ ]
Get latest updates in web technologies
I write articles related to web technologies, such as design trends, development
tools, UI/UX case studies and reviews, and more. Sign up to my newsletter to get
them all.



___
Good luck and may the force be with you!
(c) 2019 Cermati.com
